# vo-demo
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.com%2Fastron-sdc%2Fvo-demo/master?filepath=VO%20demo.ipynb)

Jupyter notebook, executable via binder, to demonstrate the usage of the ASTRON VO service.

